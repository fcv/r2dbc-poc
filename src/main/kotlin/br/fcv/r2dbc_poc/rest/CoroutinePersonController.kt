package br.fcv.r2dbc_poc.rest

import br.fcv.r2dbc_poc.PooledConnectionFactory
import br.fcv.r2dbc_poc.entity.KPerson
import br.fcv.r2dbc_poc.repository.CoroutinePersonRepository
import io.r2dbc.postgresql.api.Notification
import io.r2dbc.postgresql.api.PostgresqlConnection
import io.r2dbc.postgresql.api.PostgresqlResult
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flatMapConcat
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.reactive.asFlow
import mu.KotlinLogging
import org.springframework.http.MediaType
import org.springframework.transaction.annotation.Isolation
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono
import java.time.Duration
import java.util.Optional

private val logger = KotlinLogging.logger {}

@RestController
@RequestMapping("/persons", headers = [ "x-coroutines=true" ])
@Transactional(isolation = Isolation.REPEATABLE_READ)
class CoroutinePersonController(
    private val repository: CoroutinePersonRepository,
    connectionPoolFactory: PooledConnectionFactory
) {

    private val connection: PostgresqlConnection

    init {
        // Based on
        // https://github.com/mp911de/r2dbc-postgres-notification-example/blob/32759738866d8c972e3e1215c8778c42e7147d3d/src/main/java/com/example/demo/DemoApplication.java#L43
        connection = Mono.from(connectionPoolFactory.pool.unwrap().create())
            .cast(PostgresqlConnection::class.java)
            .block()!!
    }

    @GetMapping
    fun listAll(@RequestParam("delay") delay: Optional<Duration>): Flow<KPerson> {
        logger.debug { "listAll(delay: $delay)" }
        val persons = delay
            .filter { d: Duration -> !d.isNegative }
            .map { d: Duration ->
                val ds = d.seconds.toInt()
                logger.debug { "Querying `findAll` with a delay of $ds seconds" }
                repository.findAllWithDelay(ds)
            }
            .orElseGet { repository.findAll() }
            .map { p: KPerson ->
                // silly transformation .. just to experimenting purposes
                logger.debug { "Modifying Person instance #${p.id}" }
                val modifiedPerson = p.copy(name = p.name + "#" + p.id)
                modifiedPerson
            }
            // TODO it's not clear what the alternative for `.log` would be
            //  .log(logger.name + ".listAll.flux", Level.FINE)
        logger.debug { "About to return persons Flux" }
        return persons
    }

    @GetMapping(produces = [MediaType.TEXT_EVENT_STREAM_VALUE])
    fun streamAll(): Flow<String> {
        // Based on
        // https://github.com/pgjdbc/r2dbc-postgresql/blob/v0.8.10.RELEASE/README.md#listennotify
        // See also
        // https://github.com/mp911de/r2dbc-postgres-notification-example/blob/32759738866d8c972e3e1215c8778c42e7147d3d/src/main/java/com/example/demo/DemoApplication.java#L63-L66
        return connection.createStatement("LISTEN person_created")
            .execute()
            .asFlow()
            .flatMapConcat { postgresResult: PostgresqlResult ->
                logger.debug { "streamAll.flatMapConcat(postgresResult: $postgresResult)" }

                // TODO, it's unclear where rowsUpdated should be chained with map / flatMap or even if it's needed
                //  at all
                // postgresResult.rowsUpdated.asFlow()

                connection.notifications.asFlow()
            }
            .map { notification: Notification ->
                logger.debug { "streamAll.map(notification: $notification)" }
                "pid: ${notification.processId}, notificationName: ${notification.name}, parameter: ${notification.parameter}"
            }
            // .log(logger.name + ".streamAll", Level.FINE)
    }

    @PostMapping
    suspend fun create(@RequestBody person: KPerson): KPerson {
        logger.debug { "About to persist $person" }
        val persistedPerson = repository.save(person)
            // .log(logger.name + ".create.mono", Level.FINE)
        logger.debug { "About to return persisted person" }
        return persistedPerson
    }
}
