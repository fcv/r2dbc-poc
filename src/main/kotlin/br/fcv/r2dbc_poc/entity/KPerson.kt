package br.fcv.r2dbc_poc.entity

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table
import java.time.LocalDate

@Table("person")
data class KPerson (
    @Id
    val id: Long? = null,
    val name: String,
    val birthDate: LocalDate,
)
