package br.fcv.r2dbc_poc.repository

import br.fcv.r2dbc_poc.entity.KPerson
import kotlinx.coroutines.flow.Flow
import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.repository.kotlin.CoroutineCrudRepository

interface CoroutinePersonRepository : CoroutineCrudRepository<KPerson, Long> {
    @Query("SELECT p.* from person as p, (select pg_sleep(:delayInSeconds)) as delay")
    fun findAllWithDelay(delayInSeconds: Int): Flow<KPerson>
}
