package br.fcv.r2dbc_poc.rest;

import br.fcv.r2dbc_poc.PooledConnectionFactory;
import br.fcv.r2dbc_poc.entity.Person;
import br.fcv.r2dbc_poc.repository.PersonRepository;
import io.r2dbc.postgresql.api.PostgresqlConnection;
import io.r2dbc.postgresql.api.PostgresqlResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.Optional;
import java.util.logging.Level;

import static org.springframework.transaction.annotation.Isolation.REPEATABLE_READ;

@Slf4j

@RestController
@RequestMapping("/persons")
@Transactional(isolation = REPEATABLE_READ)
public class PersonController {

    private final PersonRepository repository;
    private final PostgresqlConnection connection;

    public PersonController(PersonRepository repository, PooledConnectionFactory connectionPoolFactory) {
        this.repository = repository;
        // Based on
        // https://github.com/mp911de/r2dbc-postgres-notification-example/blob/32759738866d8c972e3e1215c8778c42e7147d3d/src/main/java/com/example/demo/DemoApplication.java#L43
        this.connection = Mono.from(connectionPoolFactory.getPool().unwrap().create())
                .cast(PostgresqlConnection.class).block();
    }

    @GetMapping
    public Flux<Person> listAll(@RequestParam("delay") final Optional<Duration> delay) {
        log.debug("listAll(delay: {})", delay);

        Flux<Person> persons = delay
                .filter(d -> !d.isNegative())
                .map(d -> {
                    final int ds =  (int) d.getSeconds();
                    log.debug("Querying `findAll` with a delay of {} seconds", ds);
                    return repository.findAllWithDelay(ds);
                })
                .orElseGet(repository::findAll)
                .map(p -> {
                    // silly transformation .. just to experimenting purposes
                    log.debug("Modifying Person instance #{}", p.getId());
                    Person modifiedPerson = p.withName(p.getName() + "#" + p.getId());
                    return modifiedPerson;
                })
                .log(log.getName() +  ".listAll.flux", Level.FINE);
        log.debug("About to return persons Flux");
        return persons;
    }

    @GetMapping(produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<String> streamAll() {
        // Based on
        // https://github.com/pgjdbc/r2dbc-postgresql/blob/v0.8.10.RELEASE/README.md#listennotify
        // See also
        // https://github.com/mp911de/r2dbc-postgres-notification-example/blob/32759738866d8c972e3e1215c8778c42e7147d3d/src/main/java/com/example/demo/DemoApplication.java#L63-L66
        return connection.createStatement("LISTEN person_created")
                .execute()
                .flatMap(PostgresqlResult::getRowsUpdated)
                .thenMany(connection.getNotifications())
                .map(notification ->
                    String.format("pid: %s, notificationName: %s, parameter: %s", notification.getProcessId(), notification.getName(), notification.getParameter())
                )
                .log(log.getName() + ".streamAll", Level.FINE);
    }

    @PostMapping
    public Mono<Person> create(@RequestBody final Person person) {
        log.debug("About to persist {}", person);
        Mono<Person> persistedPerson = repository.save(person)
                .log(log.getName() + ".create.mono", Level.FINE);
        log.debug("About to return persisted person");
        return persistedPerson;
    }
}
