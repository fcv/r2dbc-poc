package br.fcv.r2dbc_poc;

import io.r2dbc.pool.ConnectionPool;
import io.r2dbc.pool.ConnectionPoolConfiguration;
import io.r2dbc.spi.Connection;
import io.r2dbc.spi.ConnectionFactory;
import io.r2dbc.spi.ConnectionFactoryMetadata;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.lang.NonNull;

@Slf4j
public class PooledConnectionFactory implements ConnectionFactory {

    private final ConnectionFactory factory;
    private final ConnectionPool pool;

    public PooledConnectionFactory(ConnectionFactory factory, ConnectionPoolConfiguration poolConfiguration) {
        this.factory = factory;
        this.pool = new ConnectionPool(poolConfiguration);
    }

    public ConnectionPool getPool() {
        return pool;
    }

    @NonNull
    @Override
    public Publisher<? extends Connection> create() {
        log.trace("create()");
        return pool.create();
    }

    @NonNull
    @Override
    public ConnectionFactoryMetadata getMetadata() {
        log.trace("getMetadata()");
        return factory.getMetadata();
    }

    @EventListener
    public void onContextClosed(final ContextClosedEvent event) {
        log.info("About to dispose ConnectionPool {}", pool);
        pool.dispose();
    }
}
