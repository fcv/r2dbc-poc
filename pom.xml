<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>3.3.2</version>
		<relativePath/> <!-- lookup parent from repository -->
	</parent>
	<groupId>br.fcv</groupId>
	<artifactId>r2dbc-poc</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<name>r2dbc-poc</name>
	<description>PoC project for Spring Boot with R2DBC</description>

	<properties>
		<java.version>17</java.version>
		<kotlin.version>2.0.10</kotlin.version>
	</properties>

	<dependencies>
		<dependency>
			<groupId>org.jetbrains.kotlin</groupId>
			<artifactId>kotlin-reflect</artifactId>
		</dependency>
		<dependency>
			<groupId>org.jetbrains.kotlin</groupId>
			<artifactId>kotlin-stdlib-jdk8</artifactId>
		</dependency>

		<!-- https://docs.spring.io/spring-data/r2dbc/docs/1.4.0/reference/html/#kotlin.coroutines -->
		<dependency>
			<groupId>org.jetbrains.kotlinx</groupId>
			<artifactId>kotlinx-coroutines-core</artifactId>
		</dependency>
		<dependency>
			<groupId>org.jetbrains.kotlinx</groupId>
			<artifactId>kotlinx-coroutines-reactive</artifactId>
		</dependency>
		<dependency>
			<groupId>org.jetbrains.kotlinx</groupId>
			<artifactId>kotlinx-coroutines-reactor</artifactId>
		</dependency>

		<dependency>
			<groupId>io.github.microutils</groupId>
			<artifactId>kotlin-logging-jvm</artifactId>
			<version>3.0.5</version>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-actuator</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-r2dbc</artifactId>
		</dependency>

		<!-- spring-boot-starter-web -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-webflux</artifactId>
		</dependency>

		<dependency>
			<groupId>org.postgresql</groupId>
			<artifactId>r2dbc-postgresql</artifactId>
		</dependency>

		<dependency>
			<groupId>io.r2dbc</groupId>
			<artifactId>r2dbc-pool</artifactId>
		</dependency>

		<dependency>
			<groupId>org.liquibase</groupId>
			<artifactId>liquibase-core</artifactId>
		</dependency>
		<!--
			`spring-jdbc` is required by LiquibaseAutoConfiguration at runtime. When it's not present Liquibase
			is silently disabled. So its present here is solely to enable Liquibase.
			It is **not** actually used by r2dbc.
		-->
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-jdbc</artifactId>
		</dependency>

		<dependency>
			<groupId>org.postgresql</groupId>
			<artifactId>postgresql</artifactId>
			<scope>runtime</scope>
		</dependency>
		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<optional>true</optional>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>io.projectreactor</groupId>
			<artifactId>reactor-test</artifactId>
			<scope>test</scope>
		</dependency>
	</dependencies>

	<build>
		<!--
			Based on
			https://kotlinlang.org/docs/maven.html#compile-kotlin-and-java-sources as per 2021-12-02
			and Kotlin v1.6.0
		-->
		<plugins>
			<plugin>
				<groupId>org.jetbrains.kotlin</groupId>
				<artifactId>kotlin-maven-plugin</artifactId>
				<version>${kotlin.version}</version>
				<configuration>
					<args>
						<arg>-Xjsr305=strict</arg>
					</args>
					<compilerPlugins>
						<plugin>spring</plugin>
					</compilerPlugins>
				</configuration>
				<dependencies>
					<!--
						allopen plugin aims to prevent errors like:

						> [ERROR] Tests run: 1, Failures: 0, Errors: 1, Skipped: 0, Time elapsed: 2.976 s <<< FAILURE! - in br.fcv.r2dbc_poc.R2dbcPocApplicationTests
						> [ERROR] contextLoads  Time elapsed: 0.001 s  <<< ERROR!
						> java.lang.IllegalStateException: Failed to load ApplicationContext
						> Caused by: org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'personController' defined in file [/home/fcv/projects/fcv/r2dbc-poc/target/classes/br/fcv/r2dbc_poc/rest/PersonController.class]: Initialization of bean failed; nested exception is org.springframework.aop.framework.AopConfigException: Could not generate CGLIB subclass of class br.fcv.r2dbc_poc.rest.PersonController: Common causes of this problem include using a final class or a non-visible class; nested exception is java.lang.IllegalArgumentException: Cannot subclass final class br.fcv.r2dbc_poc.rest.PersonController
						> Caused by: org.springframework.aop.framework.AopConfigException: Could not generate CGLIB subclass of class br.fcv.r2dbc_poc.rest.PersonController: Common causes of this problem include using a final class or a non-visible class; nested exception is java.lang.IllegalArgumentException: Cannot subclass final class br.fcv.r2dbc_poc.rest.PersonController
						> Caused by: java.lang.IllegalArgumentException: Cannot subclass final class br.fcv.r2dbc_poc.rest.PersonController
					-->
					<dependency>
						<groupId>org.jetbrains.kotlin</groupId>
						<artifactId>kotlin-maven-allopen</artifactId>
						<version>${kotlin.version}</version>
					</dependency>
				</dependencies>
				<executions>
					<execution>
						<id>compile</id>
						<goals>
							<goal>compile</goal>
						</goals>
						<configuration>
							<sourceDirs>
								<sourceDir>${project.basedir}/src/main/kotlin</sourceDir>
								<sourceDir>${project.basedir}/src/main/java</sourceDir>
							</sourceDirs>
						</configuration>
					</execution>
					<execution>
						<id>test-compile</id>
						<goals> <goal>test-compile</goal> </goals>
						<configuration>
							<sourceDirs>
								<sourceDir>${project.basedir}/src/test/kotlin</sourceDir>
								<sourceDir>${project.basedir}/src/test/java</sourceDir>
							</sourceDirs>
						</configuration>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<executions>
					<!-- Replacing default-compile as it is treated specially by maven -->
					<execution>
						<id>default-compile</id>
						<phase>none</phase>
					</execution>
					<!-- Replacing default-testCompile as it is treated specially by maven -->
					<execution>
						<id>default-testCompile</id>
						<phase>none</phase>
					</execution>
					<execution>
						<id>java-compile</id>
						<phase>compile</phase>
						<goals>
							<goal>compile</goal>
						</goals>
					</execution>
					<execution>
						<id>java-test-compile</id>
						<phase>test-compile</phase>
						<goals>
							<goal>testCompile</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>
</project>
